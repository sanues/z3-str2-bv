#!/usr/bin/env python

import sys
import subprocess
import xlsxwriter
import time
import os

from os import listdir, system
from os.path import isfile, join
from os import kill
from signal import alarm, signal, SIGALRM, SIGKILL
from subprocess import PIPE, Popen

#Global variables
test_config_file='z3str2_bv_test_config'
output_file='./results/Z3StrBV.xlsx'
startTimeout=20

#Defining testcase class
class TestCase(object):
	fileName=''
	timeTaken=''
	result=''
	verify=''

	def __init__(self,fileName,timeTaken,result,verify):
		self.fileName=fileName
		self.timeTaken=timeTaken
		self.result=result
		self.verify=verify



#Functions

def write_to_file():
	fh = open(executionTimeResult, "w")
	fh.write('\n'.join(executionTime))
	fh.close()

def kill_str_process():
	cmd1=['pgrep','str']
	process1 = subprocess.Popen(cmd1,stdin=subprocess.PIPE,stdout=subprocess.PIPE,stderr=subprocess.PIPE)
	output, errors = process1.communicate()
	if len(output)<=1:
		return
	pid=int(output.split('\n')[0])
	try: 
		kill(pid, SIGKILL)
	except OSError:
		pass

def get_process_children(pid):
	p = Popen('ps --no-headers -o pid --ppid %d' % pid, shell = True,stdout = PIPE, stderr = PIPE)
	stdout, stderr = p.communicate()
	return [int(p) for p in stdout.split()]

def run(args, cwd = None, shell = False, kill_tree = True, timeout = -1, env = None):
	'''
	Run a command with a timeout after which it will be forcibly
	killed.
	'''
	class Alarm(Exception):
		pass
	def alarm_handler(signum, frame):
		raise Alarm
	p = Popen(args, shell = shell, cwd = cwd, stdout = PIPE, stderr = PIPE, env = env)
	if timeout != -1:
		signal(SIGALRM, alarm_handler)
		alarm(timeout)
	try:
		stdout, stderr = p.communicate()
		if timeout != -1:
			alarm(0)
	except Alarm:
		pids = [p.pid]
		if kill_tree:
			pids.extend(get_process_children(p.pid))
		for pid in pids:
			# process might have died before getting to this line
			# so wrap to avoid OSError: no such process
			try: 
				kill(pid, SIGKILL)
			except OSError:
				pass
		return -9, '', ''
	return p.returncode, stdout, stderr

def run_z3str(fileName,timeout,sheet_name):

	testFile=test_dir+'/'+fileName

	startTime=time.time()
	
	exit_code,output,errors=run(solver_name+' '+testFile,shell=True,timeout=timeout)
	
	endTime=time.time()
	timeTaken=round(float(endTime-startTime), 3)
	
	#printing results in excel
	if len(output)==0:
		print sheet_name+'\t'+fileName+'\t\tSolver timed out!!'
		timeTaken=''
		verify=''
		result='timeout'
		kill_str_process()
	else:
		print sheet_name+'\t'+fileName+'\t\t'+str(timeTaken)
		lines=output.split('\n')
		lines = [line for line in lines if line.strip()]
		
		#To check result status
		for line in lines:			
			if 'UNSAT' in line:
				result='UNSAT'
				break;
			elif 'SAT' in line:
				result='SAT'
				break;
			elif 'UNKNOWN' in line:
				result='UNKNOWN'
				break;
			else:
				result='CRASH'
		#To check verify status
		if 'v-ok' in lines[0]:
			verify='YES'
		elif 'v-fail' in lines[0]:
			verify='NO'
		else:
			verify=''
						
	testcases.append(TestCase(fileName,timeTaken,result,verify))
	

def list_files(path):
	files = [ f for f in listdir(path) if isfile(join(path,f)) ]
	return files
	
	
def format_excel(workbook,sheet_name):
	sheet=workbook.add_worksheet(sheet_name)	
	bold = workbook.add_format({'bold': True})   # Add a bold format to use to highlight cells.
	sheet.set_column('A:A', 30)
	sheet.set_column('B:B', 15)
	sheet.set_column('C:C', 15)
	sheet.set_row(0, 30)             # set row height
	sheet.write('A1', 'Benchmark',bold)
	sheet.write('B1', 'Result',bold)
	sheet.write('C1', 'TimeTaken\n(sec)',bold)
	sheet.write('D1', 'Verified\n(sec)',bold)
	return sheet

def main():

	#Read z3str_test_config_file
	readFile=open(test_config_file,'r')
	readData= ''.join(readFile.readlines())
	readFile.close()	
	global test_dir
	global solver_name
	global testcases
	for line in readData.split('\n'):
		if line.startswith('#'):
			continue
		elif 'test_dir' in line:
			dirs=line.split('=')[1].strip('\n').split(';')
		elif 'solver' in line:
			solver_name=line.split('=')[1].strip('\n')		
	
	#writing results to excel
	if os.path.exists(output_file ):
	      os.rename(output_file,output_file[0:output_file.index('.xlsx')]+'_'+time.strftime("%c").replace(' ','_')+'.xlsx')
	workbook = xlsxwriter.Workbook(output_file)

	for test_dir in dirs:
		
		sheet_name=test_dir[test_dir.rfind('/')+1:].replace('/','')   #extracting last directory name from path
		#read all test files from test_directory
		files=list_files(test_dir)
		files.sort()
		
		testcases=[]
		for f in files:
			run_z3str(f,startTimeout,sheet_name)
			
		worksheet=format_excel(workbook,sheet_name)
		i=1
		for test in testcases:
			worksheet.write(i,0,test.fileName)
			worksheet.write(i,1,test.result)
			worksheet.write(i,2,test.timeTaken)
			worksheet.write(i,3,test.verify)
			i+=1	
	
	workbook.close()
	

if __name__ == '__main__':
	main()
